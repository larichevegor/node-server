const express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    User = require('../models/user'),
    jwt = require('jsonwebtoken');


router.post('/', (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;

    User.getUserByEmail(email, (err, user) => {
        if (err) throw err;

        if (!user) {
            return res.json({ success: false, msg: 'User not found' });
        }

        User.comparePassword(password, user.password, (err, isMatch) => {

            if (err) throw err;
            if (isMatch) {

                const token = jwt.sign({
                    // exp: 604800, // 1 week
                    data: user,
                }, '321321', { expiresIn: '100h' });

                res.json({
                    success: true,
                    token: token,
                    user: {
                        id: user.dataValues.id,
                        username: user.dataValues.username,
                        email: user.dataValues.email,
                        sex: user.dataValues.sex,
                        dateOfBirth: user.dataValues.dateOfBirth,

                    }
                });
            } else {
                return res.json({ success: false, msg: 'Wrong password' });
            }
        });
    });
});

var generateToken = function (req, res, next) {
    req.token = createToken(req.auth);
    next();
};

var sendToken = function (req, res) {
    res.json({
        token: req.token,
        user: req.auth.user
    })
}

var createToken = function (auth) {
    return token = jwt.sign({
        // exp: 604800, // 1 week
        data: auth.user,
    }, '321321', { expiresIn: '100h' });
};

// google ////////////
router.route('/google')
    .post(passport.authenticate('google-token', { session: false }), function (req, res, next) {
        if (req.user.length == 0) {
            return res.send(401, 'User Not Authenticated');
        }

        // prepare token for API
        req.auth = {
            success: true,
            user: {
                id: req.user[0].dataValues.id,
                username: req.user[0].dataValues.username,
                email: req.user[0].dataValues.email,
                avatar: req.user[0].dataValues.avatar
            }
        };

        next();
    }, generateToken, sendToken);

// twitter ////////////
router.route('/twitter')
    .post(passport.authenticate('twitter-token', { session: false }), function (req, res, next) {
        if (req.user.length == 0) {
            return res.send(401, 'User Not Authenticated');
        }

        // prepare token for API
        req.auth = {
            success: true,
            user: {
                id: req.user[0].dataValues.id,
                username: req.user[0].dataValues.username,
                email: req.user[0].dataValues.email,

            }
        };

        next();
    }, generateToken, sendToken);

// facebook ////////////
router.route('/facebook')
    .post(passport.authenticate('facebook-token', { session: false }), function (req, res, next) {
        if (req.user.length == 0) {
            return res.send(401, 'User Not Authenticated');
        }

        // prepare token for API
        req.auth = {
            success: true,
            user: {
                id: req.user[0].dataValues.id,
                username: req.user[0].dataValues.username,
                email: req.user[0].dataValues.email,

            }
        };

        next();
    }, generateToken, sendToken);




module.exports = router;