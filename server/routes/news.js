const express = require('express'),
    router = express.Router(),
    NEWS = require('../models/news'),
    jwt = require('jsonwebtoken'),
    multer = require('multer'),
    upload = multer({ dest: 'public/images/' }),
    TAGS = require('../models/tags'),
    models = require('../models');

router.post('/', (req, res, next) => {
    var decoded = jwt.decode(req.body.token, { complete: true });
    let id = decoded.payload.data.id.toString();
    let name = decoded.payload.data.username.toString();
    let newNews = new models.news({
        authorname: name,
        newsText: req.body.newsText,
        authorId: id,
        newsTitle: req.body.newsTitle
    });

    let tags = req.body.tags.split(",");

    let savedNews = NEWS.addNews(newNews);
    savedNews
        .then(dbRes => {
            models.tags.findAll({ where: { id: tags } }).then((newTags) => {
                dbRes.addTag(newTags, { taggable: 'news' }).then(() => {
                    res.json({ success: true, msg: 'models.news added successful' });
                })
            })
        })
        .catch(err => {
            // console.log('###err', err);
            res.json({ success: false, msg: 'Failed to add news' });
        });
});

//  models.news.create(newNews).then((news) => {profile
//         Tags.findAll({ where: { id: tags } }).then((newTags) => {
//             news.addTag(newTags, { taggable: 'news' }).then(() => {
//                 res.json({ success: true, msg: 'models.news added successful' });
//             })
//         })
//     })


router.post('/img', upload.single('avatar'), function (req, res, next) {
    let avatarpath = 'http://localhost:1337' + req.file.path.substring(6, req.file.path.length)
    var decoded = jwt.decode(req.body.token, { complete: true });
    let id = decoded.payload.data.id.toString();
    let name = decoded.payload.data.username.toString();
    let newNews = new models.news({
        authorname: name,
        newsText: req.body.newsText,
        authorId: id,
        avatar: avatarpath,
        newsTitle: req.body.newsTitle
    });
    // console.log(req.body.tags);
    let tags = req.body.tags.split(",");

    let savedNews = NEWS.addNews(newNews);
    savedNews
        .then(dbRes => {
            models.tags.findAll({ where: { id: tags } }).then((newTags) => {
                dbRes.addTag(newTags, { taggable: 'news' }).then(() => {
                    res.json({ success: true, msg: 'models.news added successful' });
                })
            })
        })
        .catch(err => {
            // console.log('###err', err);
            res.json({ success: false, msg: 'Failed to add news' });
        });
})




router.delete('/', (req, res, next) => {
    let token = req.body.token;
    let newsId = req.body.newsId;
    let savedNews = NEWS.deleteNews(token, newsId);

    savedNews
        .then(dbRes => {
            if (dbRes == 1)
                res.json({ success: true, msg: 'models.news delete successful' });
            if (dbRes == 0)
                res.json({ success: false, msg: 'Failed' });
        })
        .catch(err => {
            res.json({ success: false, msg: 'Error' });
        });
});

router.get('/author/:id', (req, res, next) => {
    let id = req.params.id;
    models.news.findAll({
        where:
            {
                authorId: id
            },
        include: [{
            model: models.tags,
        }]
    }).then(dbRes => {

        return res.json(dbRes);
    })

});






router.get('/', (req, res, next) => {
    models.news.findAll({
        include: [{
            model: models.Tags,
        }]
    }).then(dbRes => {
        return res.json(dbRes);
    })
});

router.put('/:id', (req, res, next) => {
    var decoded = jwt.decode(req.body.token, { complete: true });
    let id = decoded.payload.data.id.toString();

    let newsId = req.params.id;
    let tags = req.body.tags.split(",");



    const query = {
        where: {
            id: newsId,
            authorId: id,
        }
    }

    models.news.find(query).then(function (news) {
        if (news) {
            news.updateAttributes({
                newsText: req.body.newsText,
                newsTitle: req.body.newsTitle
            })
            models.tags.findAll({ where: { id: tags } }).then((newTags) => {
                news.setTags([], { taggable: 'news' }).then(() => {
                    news.addTag(newTags, { taggable: 'news' }).then(() => {
                        res.json({ success: true, msg: 'models.news added successful' });
                    })
                })

            })
        }
    })




    // updatedNews
    //     .then(dbRes => {
    //            console.log('dbRes',dbRes)
    //         Tags.findAll({ where: { id: tags } }).then((newTags) => {
    //             // console.log(newTags)
    //             dbRes.addTag(newTags, { taggable: 'news' }).then(() => {
    //                 res.json({ success: true, msg: 'models.news added successful' });
    //             })
    //         })
    //     })
    //     .catch(err => {
    //         console.log('###err', err);
    //         res.json({ success: false, msg: 'Error' });
    //     });

});

router.put('/img/:id', upload.single('avatar'), (req, res, next) => {
    // console.log(123123123);
    let avatarpath = 'http://localhost:1337' + req.file.path.substring(6, req.file.path.length)
    var decoded = jwt.decode(req.body.token, { complete: true });
    let id = decoded.payload.data.id.toString();
    let newsId = req.params.id;

    let tags = req.body.tags.split(",");

    const query = {
        where: {
            id: newsId,
            authorId: id,
        }
    }

    models.news.find(query).then(function (news) {
        if (news) {
            news.updateAttributes({
                newsText: req.body.newsText,
                newsTitle: req.body.newsTitle,
                avatar: avatarpath
            })
            models.tags.findAll({ where: { id: tags } }).then((newTags) => {
                news.setTags([], { taggable: 'news' }).then(() => {
                    news.addTag(newTags, { taggable: 'news' }).then(() => {
                        res.json({ success: true, msg: 'models.news changed successful' });
                    })
                })

            })
        }
    });
});


router.post('/tag', (req, res, next) => {
    // console.log(new Tags);
    let newTags = new models.tags({
        tag: req.body.tag
    });
    // console.log('asdasdasdadsadsa', newTags);

    let savedTags = TAGS.addTags(newTags);

    savedTags
        .then(dbRes => {
            // console.log('###dbRes', dbRes);
            res.json({ dbRes: dbRes, success: true, msg: 'Tag added successful' });
        })
        .catch(err => {
            // console.log('###err', err);
            res.json({ success: false, msg: 'Failed to register user' });
        });
});

router.get('/tags', (req, res, next) => {
    models.tags.getAllTags((err, tags) => {
        if (err) throw err;
        if (tags) {
            return res.json(tags);
        }
    });
});



router.get('/search', (req, res, next) => {
    let tags, text, id;
    if (req.headers.tags != undefined) {
        tags = req.headers.tags.split(",");
    }
    if (req.query.text != undefined) {
        text = '%' + req.query.text + '%'
    }
    if (req.query.id != undefined) {
        id = req.query.id
    }
    let idofNews = [];

    function getId(news) {
        // idofNews = []
        idofNews.length = 0;
        for (let i = 0; i < news.length; i++) {
            idofNews.push(news[i].dataValues.id)
        }
        return idofNews;
        // if(typeof news.length !== 'undefined') {
        //   idofNews = news.map(n => n.id);
        // }
        // console.log(99999, idofNews);
        // return idofNews;
    }

    // queries
    let querySearchByAuthor = {
        where: {
            authorId: id,
        },
        attributes: ['id']
    };

    let quarySearchAll = {
        attributes: ['id']
    };

    let querySearchByTags = {
        where: {
            id: idofNews
        },
        include: [{
            model: models.tags,
            where:
                {
                    id: tags
                }
        }],
    };

    let querySearchByWord = {
        where: {
            $and: [{
                id: idofNews,
            },
            {
                $or: [
                    {
                        newsTitle: {
                            $iLike: text
                        }
                    },
                    {
                        newsText: {
                            $iLike: text

                        }
                    }
                ]
            }
            ],
        },
    };

    let queryAddLastTask =
        {
            where: {
                id: idofNews
            },
            include: [{
                model: models.tags,
            }]
        };

    /////// На странице юзера ////////////
    // С поиском без тэгов с юзером
    if (req.query.text != undefined && req.headers.tags == undefined && req.query.id != undefined) {
        models.news.findAll(querySearchByAuthor).then(news => {
            querySearchByWord.where.id = getId(news);
            models.news.findAll(querySearchByWord).then(news => {
                queryAddLastTask.where.id = getId(news);
                models.news.findAll(queryAddLastTask).then(dbRes => {
                    // console.log('1, dbRes', dbRes)
                    // console.log('1', idofNews)
                    if (dbRes.length == 0) {
                        return res.json(dbRes)
                    }
                    return res.json(dbRes);
                })
            })
        })
    }


    // С поиском с тэгами, с юзером
    if (req.query.text != undefined && req.headers.tags != undefined && req.query.id != undefined) {
        models.news.findAll(querySearchByAuthor).then((news) => {
            querySearchByWord.where.id = getId(news);
            models.news.findAll(querySearchByWord).then((news) => {
                querySearchByTags.where.id = getId(news);
                models.news.findAll(querySearchByTags).then((news) => {
                    queryAddLastTask.where.id = getId(news);
                    models.news.findAll(queryAddLastTask).then(dbRes => {
                        if (dbRes.length == 0) {
                            return res.json(dbRes)
                        }
                        return res.json(dbRes);
                    })
                })
            })
        })
    }

    // Без поиска с тэгами с юзером
    if (req.query.text == undefined && req.headers.tags != undefined && req.query.id != undefined) {
        models.news.findAll(querySearchByAuthor).then((news) => {
            querySearchByTags.where.id = getId(news);
            models.news.findAll(querySearchByTags).then(news => {
                queryAddLastTask.where.id = getId(news);
                models.news.findAll(queryAddLastTask).then(dbRes => {
                    if (dbRes.length == 0) {
                        return res.json(dbRes)
                    }
                    return res.json(dbRes);
                })
            })
        })
    }
    /////////////////////////////


    ////////////////// На главной ///////////////


    // С поиском без тэгов  без юзера
    if (req.query.text != undefined && req.headers.tags == undefined && req.query.id == undefined) {
        models.news.findAll(quarySearchAll).then(news => {
            querySearchByWord.where.id = getId(news);
            models.news.findAll(querySearchByWord).then(news => {
                queryAddLastTask.where.id = getId(news);
                models.news.findAll(queryAddLastTask).then(dbRes => {
                    if (dbRes.length == 0) {
                        return res.json(dbRes)
                    }
                    return res.json(dbRes);
                })
            })
        })
    }

    // С поиском с тэгами без юзера
    if (req.query.text != undefined && req.headers.tags != undefined && req.query.id == undefined) {
        models.news.findAll(quarySearchAll).then((news) => {
            querySearchByWord.where.id = getId(news);
            models.news.findAll(querySearchByWord).then((news) => {
                querySearchByTags.where.id = getId(news);
                models.news.findAll(querySearchByTags).then(news => {
                    queryAddLastTask.where.id = getId(news);
                    models.news.findAll(queryAddLastTask).then(dbRes => {
                        if (dbRes.length == 0) {
                            return res.json(dbRes)
                        }
                        return res.json(dbRes);
                    })
                })
            })
        })

    }


    // Без поиска с тэгами без юзера
    if (req.query.text == undefined && req.headers.tags != undefined && req.query.id == undefined) {
        models.news.findAll(quarySearchAll).then((news) => {
            querySearchByTags.where.id = getId(news);
            models.news.findAll(querySearchByTags).then(news => {
                queryAddLastTask.where.id = getId(news);
                models.news.findAll(queryAddLastTask).then(dbRes => {
                    if (dbRes.length == 0) {
                        return res.json(dbRes)
                    }
                    return res.json(dbRes);
                })
            })
        })

    }

});

module.exports = router;