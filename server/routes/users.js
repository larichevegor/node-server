const express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    User = require('../models/user'),
    jwt = require('jsonwebtoken'),
    multer = require('multer'),
    upload = multer({ dest: 'public/images/' }),
    // connection.import('../models/user');
    // let Users = connection.models.users;
    // let Interests = connection.models.interests;
    models = require('../models');


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'imgs/')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
    }
})

router.put('/setimg', upload.single('avatar'), function (req, res, next) {
    var avatarpath = 'http://localhost:1337' + req.file.path.substring(6, req.file.path.length)
    var decoded = jwt.decode(req.headers.token, { complete: true });
    var id = decoded.payload.data.id.toString();
    var query = {
        where: {
            id: id
        }
    }

    models.users.find(query).then(function (user) {
        if (user) {
            user.updateAttributes({
                avatar: avatarpath
            })
        }
    })
})





// Register
router.post('/register', (req, res, next) => {
    let newUser = new models.users({
        username: req.body.userData.username,
        password: req.body.userData.password,
        email: req.body.userData.email,

    });
    
    let check = User.uniqueCheck(newUser.email);

    check
        .then(dbRes => {
            if (dbRes == null) {
                let savedUser = User.addUser(newUser);
                savedUser
                    .then(dbRes => {
                        res.json({ success: true, msg: 'User registered successful' });
                    })
                    .catch(err => {
                        res.json({ success: false, msg: 'Failed to register user' });
                    });
            }
            else {
                res.json({ success: false, msg: 'User already exist' });
            }
        })
        .catch(err => {
            res.json({ success: false, msg: 'Failed to register user' });
        });



});



// Profile
router.get('/profile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    res.json({ user: req.user });
});

router.get('/profile/:id', (req, res, next) => {
    let id = req.params.id;
    User.getProfileForUser(id, (err, user) => {
        if (err) throw err;
        if (user) {
            return res.json(user);
        }
    });
});

router.get('/interests/', (req, res, next) => {
    models.interests.findAll().then(data => {
        res.json(data)
    })
});



router.put('/', (req, res, next) => {
    let body = req.body
    let updatedUser = User.changeProfileUser(body);

    updatedUser
        .then(dbRes => {
            res.json({ success: true, msg: 'User profile update successful' });
        })
        .catch(err => {
            res.json({ success: false, msg: 'Error' });
        });
});





module.exports = router;