const app = require('../server')
    , chaiHttp = require('chai-http')
    , chai = require('chai')
    , expect = chai.expect
    , models = require('../models')
    , testConfig = require('./testconfig');


chai.use(chaiHttp);

describe('Users', () => {
    const userData = testConfig.userData
    before((done) => {
        models.users.destroy({
            where: {
                email: userData.email
            },
            truncate: true,
            cascade: true
        });
        done();
    });
    describe('user login', function () {
        it('Should register user ', function (done) {
            chai.request(app).post('/users/register')
                .set('Content-Type', 'application/json')
                .send({ userData })
                .end(function (err, res) {
                    expect(res.body).have.property('success', true)
                    expect(res).have.status(200)
                    // res.should.have.status(200);
                    // res.body.should.have.property('success').eql(true);
                    done();
                });
        });

    });

    describe('user auth', function () {
        it('Should auth user ', function (done) {
            chai.request(app).post('/auth')
                .set('Content-Type', 'application/json')
                .send({ email: userData.email, password: userData.password })
                .end(function (err, res) {
                    expect(res.body).have.property('success', true)
                    expect(res).have.status(200)
                    expect(res.body.token).to.be.a('string')
                    done();
                });
        });
    });



})

