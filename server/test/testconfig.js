module.exports = config = {
    userData: {
        username: 'TestUser',
        password: 'testpassword',
        email: 'testemail1',
        dateOfBirth: 'testDateOfBirth',
        sex: 'male'
    },
    Polls: {
        data: {
            description: 'test',
            votesCard: ['nice', 'life']
        },
        interests: ['1', '2'],
        email: 'testemail',
        photoPath: ["/home/fusion/Desktop/node-server/server/pic1.jpg", "/home/fusion/Desktop/node-server/server/pic2.jpg"]
    },
    testUser: {
        username: 'TestUser1',
        password: 'testpassword',
        email: 'testemail',
        dateOfBirth: 'testDateOfBirth',
        sex: 'male'
    },
};