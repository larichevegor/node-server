const app = require('../server')
    , chaiHttp = require('chai-http')
    , chai = require('chai')
    , expect = chai.expect
    , models = require('../models')
    , testConfig = require('./testconfig');

chai.use(chaiHttp);

describe('Polls', () => {
    const Polls = testConfig.Polls
    var token;

    before((done) => {
        const userData = testConfig.userData
        chai.request(app).post('/auth')
            .set('Content-Type', 'application/json')
            .send({ email: userData.email, password: userData.password })
            .end(function (err, res) {
                expect(res.body).have.property('success', true);
                expect(res).have.status(200);
                expect(res.body.token).to.be.a('string');
                console.log('here we check', res.body.token);
                token = res.body.token;
                done();
            });
    });


describe("getting all polls", function () {
    it("Should get polls", function (done) {
        chai.request(app).get("/polls")
            .end(function (err, res) {
                expect(res).have.status(200);
                done();
            });
    })
})

describe('create poll by auth user', function () {
    console.log('token', token)
    it('Should add polls ', function (done) {
        chai.request(app).post('/polls')
            .set('Content-Type', 'multipart/form-data')
            .set('Authorization', token)
            .field('data', '{"description":"' + Polls.data.description + '", "votesCard":"' + Polls.data.votesCard.toString() + '"}')
            .field('interests', Polls.interests.toString())
            .attach('photo', Polls.photoPath[0])
            .attach('photo', Polls.photoPath[1])
            .end(function (err, res) {
                expect(res.body).have.property('success', true);
                expect(res).have.status(200);
                done();
            });
    });

});

describe('create poll by unauth user', function () {
    it('Shouldnt add polls ', function (done) {
        chai.request(app).post('/polls')
            .set('Content-Type', 'multipart/form-data')
            .field('data', '{"description":"' + Polls.data.description + '", "votesCard":"' + Polls.data.votesCard.toString() + '"}')
            .field('interests', Polls.interests.toString())
            .attach('photo', Polls.photoPath[0])
            .attach('photo', Polls.photoPath[1])
            .end(function (err, res) {
                expect(res.body).have.property('success', false);
                expect(res).have.status(200);
                done();
            });
    });

});

after((done) => {
    models.polls.destroy({ truncate: true, cascade: true });
    done();
});

});
