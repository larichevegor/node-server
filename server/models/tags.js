var Tags;
module.exports = function (sequelize, DataTypes) {
   Tags = sequelize.define('tags',
    {
      tag:
      {
        type: DataTypes.TEXT,

      },
    })
  return Tags;
}


module.exports.addTags = function (newTags, callback) {
  return new Promise((resolve, reject) => {
    resolve(newTags.save());
  });
}

module.exports.getAllTags = function (callback) {
  Tags
    .findAll()
    .then(dbRes => callback(null, dbRes))
    .catch(err => callback(err));
}

