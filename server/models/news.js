const jwt = require('jsonwebtoken');

var News;
module.exports = function (sequelize, DataTypes) {
  News = sequelize.define('news',
    {
      authorname:
      {
        type: DataTypes.TEXT,
      },
      newsTitle:
      {
        type: DataTypes.TEXT,

      },
      newsText:
      {
        type: DataTypes.TEXT,

      },
      authorId:
      {
        type: DataTypes.TEXT,
      },  
      avatar:
      {
        type: DataTypes.TEXT,
      },

    })

  return News;
}



module.exports.addNews = function (newNews, callback) {
  return new Promise((resolve, reject) => {
    resolve(newNews.save());
  });
}

module.exports.getAllNews = function (callback) {
  News
    .findAll()
    .then(dbRes => callback(null, dbRes))
    .catch(err => callback(err));
}


// module.exports.getNewsForUser = function (id, callback) {
//   console.log(id);
//   const query = {
//     where: {
//       authorId: id
//     }
//   }
//   console.log(query);




//     .catch(err => callback(err));

//   NEWS
//     .findAll(query)
//     .then(dbRes => callback(null, dbRes))
//     .catch(err => callback(err));
// }



module.exports.deleteNews = function (token, idOfNews, callback) {
  var decoded = jwt.decode(token, { complete: true });
  let id = decoded.payload.data.id.toString();
  const query = {
    where: {
      id: idOfNews,
      authorId: id,
    }
  }
  
  return new Promise((resolve, reject) => {
    resolve(News.destroy(query));
  });
}

// module.exports.changeNews = function (idOfNews, id, body, callback) {
//   var decoded = jwt.decode(body.token, { complete: true });
//   let id = decoded.payload.data.id.toString();

//   const query = {
//     where: {
//       id: idOfNews,
//       authorId: id,
//     }
//   }


//   new Promise((resolve, reject) => {
//     resolve(
//       NEWS.find(query).then(function (news) {
//         if (news) {
//           news.updateAttributes({
//             newsText: body.newsText,
//             newsTitle: body.newsTitle
//           })
//         }
//       })
//     );
//   });

//   return NEWS
//     .findAll(query)
//     .then(dbRes => callback(null, dbRes))
//     .catch(err => callback(err));



// }





