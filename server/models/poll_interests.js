var PollInteresRel;
module.exports = function (sequelize, DataTypes) {
  PollInteresRel = sequelize.define('pollinteresrel',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      interes_id: {
        type: DataTypes.INTEGER,
        unique: 'pollinteresrel_interesable'
      },
      interesable: {
        type: DataTypes.STRING,
        unique: 'pollinteresrel_interesable'
      },
      interesable_id: {
        type: DataTypes.INTEGER,
        unique: 'pollinteresrel_interesable',
        references: null
      }

    })
  return PollInteresRel;
}