
var UserInteresRel;
module.exports = function (sequelize, DataTypes) {
  UserInteresRel = sequelize.define('userinteresrel',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      interes_id: {
        type: DataTypes.INTEGER,
        unique: 'userinteresrel_interesable'
      },
      interesable: {
        type: DataTypes.STRING,
        unique: 'userinteresrel_interesable'
      },
      interesable_id: {
        type: DataTypes.INTEGER,
        unique: 'userinteresrel_interesable',
        references: null
      }

    })

  return UserInteresRel;
}