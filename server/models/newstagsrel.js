var NewsTagsRel;
module.exports = function (sequelize, DataTypes) {
  NewsTagsRel = sequelize.define('newstagsrel',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      tag_id: {
        type: DataTypes.INTEGER,
        unique: 'newstagsrel_taggable'
      },
      taggable: {
        type: DataTypes.STRING,
        unique: 'newstagsrel_taggable'
      },
      taggable_id: {
        type: DataTypes.INTEGER,
        unique: 'newstagsrel_taggable',
        references: null
      }

    })

  return NewsTagsRel; 
}



module.exports.addNewsTagsRel = function (NewsTagsRel, callback) {
  return new Promise((resolve, reject) => {
    resolve(NewsTagsRel.save());
  });
}
