var Vote;
module.exports = function (sequelize, DataTypes) {
     Vote = sequelize.define('vote',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            id_poll: {
                type: DataTypes.INTEGER,
            },
            id_owner: {
                type: DataTypes.INTEGER,
            },
            id_result:
            {
                type: DataTypes.INTEGER,

            },  
        })

    return Vote;
}