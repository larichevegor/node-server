const Sequelize = require('sequelize');
  
const connection = new Sequelize('fusion', 'fusion', 'fusion', {
    host: 'localhost',
    dialect: 'postgres',
    logging: false,
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    },
  });

  module.exports = connection;